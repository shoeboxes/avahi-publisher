FROM armhf/alpine:3.5
RUN apk add --no-cache --virtual --update openrc dbus avahi avahi-tools inotify-tools && \
    echo -e '#!/bin/sh\nrm /var/run/avahi-daemon/pid' > /etc/local.d/cleanup.start && \
    # get various things ready for OpenRC to work...
    sed -i '/tty/d' /etc/inittab && \
    sed -i 's/#rc_sys=""/rc_sys="docker"/g' /etc/rc.conf && \
    echo 'rc_provide="loopback net"' >> /etc/rc.conf && \
    sed -i 's/^#\(rc_logger="YES"\)$/\1/' /etc/rc.conf && \
    sed -i 's/hostname $opts/# hostname $opts/g' /etc/init.d/hostname && \
    sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh && \
    sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh
ADD etc/init.d/avahi-aliases /etc/init.d/avahi-aliases
RUN rc-update add local && \
    rc-update add avahi-aliases
ADD avahi-aliases.tmpl /templates/avahi-aliases.tmpl
ADD avahi-alias.sh /avahi-alias.sh
VOLUME /config /templates
EXPOSE 5353/udp
CMD ["/sbin/init"]

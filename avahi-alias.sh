#!/bin/sh

trap : SIGTERM SIGINT SIGHUP

get_ip() {
  ifconfig $(route | grep "^default" | awk '{ print $8 }') | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1 }'
}

monitor_ip() {
  while true
  do
    if [ $1 != $(get_ip) ]
    then
      kill -HUP $$
      exit
    fi

    sleep 5
  done
}

monitor_config() {
  inotifywait -q -e modify $1
  kill -HUP $$
}

while true
do
  PIDS=""
  DEFAULT_IP=$(get_ip)

  for ALIAS in $(cat $1)
  do
    echo publishing: $ALIAS -\> $DEFAULT_IP
    avahi-publish -a -R $ALIAS $DEFAULT_IP &> /dev/null &
    PIDS="$PIDS $!"
  done

  monitor_ip $DEFAULT_IP &
  PIDS="$PIDS $!"

  monitor_config $1 &
  PIDS="$PIDS $!"

  wait
  SIGNAL=$?

  echo un-publishing all aliases...
  kill $PIDS
  echo done

  if [ $SIGNAL -ne 129 ]
  then
    echo exiting
    exit
  fi
done
